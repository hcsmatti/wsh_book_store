package cz.equahatchery.wshbookstore.model;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author WSH
 */
public interface Basket extends Serializable {

    /**
     * Book - quantity pair object
     */
    public class BookItem {

        private BookEntity book;
        private int quantity;

        public BookItem(BookEntity book, int quantity) {
            this.book = book;
            this.quantity = quantity;
        }

        //<editor-fold defaultstate="collapsed" desc="getters setters">
        public BookEntity getBook() {
            return book;
        }

        public void setBook(BookEntity book) {
            this.book = book;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
        //</editor-fold>        
    }

    /**
     * User details data object
     */
    public class UserDetails {

        private String name;
        private String address1;
        private String address2;
        private String city;
        private String zip;
        private String state;
        private String phone;
        private String email;

        //<editor-fold defaultstate="collapsed" desc="getters setters">
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
        //</editor-fold>           
    }

    /**
     * Inserts book into the basket. If book was already present, quantity will
     * be overwritten.
     *
     * @param be book to be inserted into the basket
     * @param quantity number of books in basket
     * @return
     */
    public void putItem(BookEntity be, int quantity);

    /**
     * Returns list of all books and their quantities.
     *
     * @return list of book - quantity pairs in the basket
     */
    public Collection<BookItem> getBookItemCollection();

    /**
     * Returns count of all items present in the basket.
     *
     * @return count of all items present in the basket.
     */
    public int getItemCount();

    /**
     * Returns number of books of this id present in the basket.
     *
     * @param bookId id of book to be counted
     * @return number of books of this id present in the basket
     */
    public int getItemQuantity(long bookId);

    /**
     * Removes all books of the specified id from the basket.
     *
     * @param bookId
     * @return true if book of specified id was present in the basket
     */
    public boolean removeItem(long bookId);

    /**
     * Returns total price of all the items in the basket.
     *
     * @return total price of the basket
     */
    public int getTotal();

    /**
     * Inserts user details to basket
     * @param userDetails contents of user shipping info form
     */
    public void setUserDetails(UserDetails userDetails);
    
    /**
     * Returns user details of the basket.
     * @return user details form content
     */
    public UserDetails getUserDetails();
    
    /**
     * Validates order if it can be sent in it's current state.
     * @return true if order can be sent
     */
    public boolean validateOrder();
    
    /**
     * Sends order for processing
     * @return true if order was acknowleged from remote system
     */
    public boolean sendOrder();
}
