package cz.equahatchery.wshbookstore.model;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Basket Implementation
 *
 * @author WSH
 */
@Component
@Scope("session")
public class BasketImpl implements Basket {

    private Map<Long, BookItem> items;
    private int itemCount = 0;
    private int total = 0;
    private UserDetails userDetails;

    public BasketImpl() {
        items = new LinkedHashMap<Long, BookItem>();
        userDetails = new UserDetails();
    }

    public void putItem(BookEntity be, int quantity) {

        if (quantity > 0) {
            items.put(be.getId(), new BookItem(be, quantity));

            updateBasketValues();
        } else {
            removeItem(be.getId());
        }
    }

    public Collection<BookItem> getBookItemCollection() {
        return items.values();
    }

    public int getItemCount() {
        return itemCount;
    }

    public int getItemQuantity(long bookId) {
        if (items.containsKey(bookId)) {
            return items.get(bookId).getQuantity();
        } else {
            return 0;
        }
    }

    public boolean removeItem(long bookId) {
        BookItem book = items.remove(bookId);
        updateBasketValues();
        return book != null;
    }

    public int getTotal() {
        return total;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public boolean validateOrder() {
        //TODO
        return itemCount != 0;
    }

    public boolean sendOrder() {
        //TODO 
        return true;
    }

    private void updateBasketValues() {
        itemCount = 0;
        total = 0;

        for (BookItem book : items.values()) {
            itemCount += book.getQuantity();
            total += book.getQuantity() * book.getBook().getPrice();
        }
    }
}
