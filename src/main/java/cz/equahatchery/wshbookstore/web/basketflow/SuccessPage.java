/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.equahatchery.wshbookstore.web.basketflow;

import cz.equahatchery.wshbookstore.web.homepage.HomePage;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.link.Link;

/**
 *
 * @author vmarcan
 */
public final class SuccessPage extends BasketFlow {

    public SuccessPage() {
        super();
                add(new Link("continueShopping") {
            @Override
            public void onClick() {
                setResponsePage(HomePage.class);
            }
        }); 
    }
    
    public SuccessPage(PageParameters params) {
        //TODO:  process page parameters
    }
}
