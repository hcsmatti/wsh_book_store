/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.equahatchery.wshbookstore.web.basketflow;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.ResourceModel;

/**
 *
 * @author vmarcan
 */
public final class ShipDetailsPage extends BasketFlow {

    public ShipDetailsPage() {
        super();
        add(new Label("shipping", new ResourceModel("shipDetailsPage.title")));
        
        
        
        
        add(new Link("itemsPage") {
            @Override
            public void onClick() {
                setResponsePage(ItemsPage.class);
            }
        });
        add(new Link("confirm") {
            @Override
            public void onClick() {
                setResponsePage(SuccessPage.class);
            }
        });

    }

    public ShipDetailsPage(PageParameters params) {
        //TODO:  process page parameters
    }
}
