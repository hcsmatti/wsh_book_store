
package cz.equahatchery.wshbookstore.web.basketflow;

import cz.equahatchery.wshbookstore.model.Basket;
import cz.equahatchery.wshbookstore.model.Basket.BookItem;
import cz.equahatchery.wshbookstore.web.homepage.HomePage;
import java.util.Collection;
import java.util.Iterator;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author vit marcan
 */
public final class ItemsPage extends BasketFlow {
    
    //bean with basket items
    @SpringBean
    private Basket basket;
    
    public ItemsPage() {
        super();
        add(new Label("contents", new ResourceModel("itemsPage.title")));
        
        final Collection<BookItem> basketContents = basket.getBookItemCollection();       
        
        
        Iterator<BookItem> books = basketContents.iterator();
        RepeatingView repeating = new RepeatingView("repeating");
        add(repeating);
        
        while (books.hasNext()){
            BookItem book = books.next();                    
            repeating.add(new Label("bookName", book.getBook().getName()));
            repeating.add(new Label("bookQuantity", String.valueOf(book.getQuantity())));
        }
        

    
            
        //link to next page
        add(new Link("ShipDetailsPage") {
            @Override
            public void onClick() {
                setResponsePage(ShipDetailsPage.class);
            }
        });
        
        //link back
        add(new Link("continueShopping") {
            @Override
            public void onClick() {
                setResponsePage(HomePage.class);
            }
        });      
    }
    
    public ItemsPage(PageParameters params) {
        //TODO:  process page parameters
    }
}
