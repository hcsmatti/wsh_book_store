/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.equahatchery.wshbookstore.web.basketflow;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.ResourceModel;

/**
 *
 * @author vit marcan
 */
public final class ConfirmPage extends BasketFlow {

    public ConfirmPage() {
        super();
        add(new Label("confirm", new ResourceModel("confirmPage.title")));
        add(new Link("shipDetails") {
            @Override
            public void onClick() {
                setResponsePage(ShipDetailsPage.class);
            }
        });
        add(new Link("success") {
            @Override
            public void onClick() {
                setResponsePage(SuccessPage.class);
            }
        });
        
    getSession().invalidate();
    }
    
    public ConfirmPage(PageParameters params) {
        //TODO:  process page parameters
    }
}
