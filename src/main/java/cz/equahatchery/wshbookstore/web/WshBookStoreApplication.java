package cz.equahatchery.wshbookstore.web;

import cz.equahatchery.wshbookstore.model.Basket;
import cz.equahatchery.wshbookstore.model.BasketImpl;
import cz.equahatchery.wshbookstore.web.author.AuthorListPage;
import cz.equahatchery.wshbookstore.web.basketflow.ItemsPage;
import cz.equahatchery.wshbookstore.web.basketflow.SuccessPage;
import cz.equahatchery.wshbookstore.web.book.GridTestPage;
import cz.equahatchery.wshbookstore.web.homepage.HomePage;
import org.apache.wicket.Page;
import org.apache.wicket.Request;
import org.apache.wicket.Response;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.stereotype.Component;

/**
 * Main Wicket Controller
 * @author WSH
 */
@Component("wshBookStoreApplication")
public class WshBookStoreApplication extends WebApplication {

    @SpringBean
    public static Basket basket;

    /**
     * Constructor
     */
    public WshBookStoreApplication() {
       mountBookmarkablePage("HomePage", HomePage.class);
       mountBookmarkablePage("AuthorListPage", AuthorListPage.class);
       mountBookmarkablePage("GridTestPage", GridTestPage.class);
        mountBookmarkablePage("ItemsPage", ItemsPage.class);
        mountBookmarkablePage("SuccessPage", SuccessPage.class);
//        mountBookmarkablePage("ValueSetsListPage", ValueSetsListPage.class);
//        mountBookmarkablePage("DemoPage", DemoPage.class);
        //mountBookmarkablePage("/DataElementConceptAdd", DataElementConceptDetailPage.class);
        //mountBookmarkablePage("/UserAdd", UserDetailPage.class);
        
        


    }

    /**
     * Define home (welcome) page
     * @return HomePage 
     */
    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    /**
     * To wire spring enviroment into wicket framework
     */
    @Override
    protected void init() {
        super.init();
        addComponentInstantiationListener(new SpringComponentInjector(this));
       
    }

    /**
     * Session inicialisation
     * @param request HttpRequest
     * @param response HttpResponse
     * @return WshBookStoreSession for connected client
     */
    @Override
    public Session newSession(Request request, Response response) {
        basket = new BasketImpl();
        return new WshBookStoreSession(request);
    }
}