package cz.equahatchery.wshbookstore.web.author;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.repository.AuthorRepository;
import cz.equahatchery.wshbookstore.web.BasePage;
import java.text.SimpleDateFormat;
import org.apache.wicket.Request;
import org.apache.wicket.ResourceReference;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author Jan Jedlicka <jedlija@gmail.com>
 */
public class AuthorDetailPage extends BasePage {

    @SpringBean
    private AuthorRepository repository;
    private AuthorEntity author;
    private boolean fullCv;

    //Public contructor for external access
    public AuthorDetailPage(Long authorId) {
        super();
        author = repository.get(AuthorEntity.class, authorId);
        fullCv = false;

        construct();
    }

    //Private constructor for full CV printout
    private AuthorDetailPage(Long authorId, boolean fullCv) {
        super();
        author = repository.get(AuthorEntity.class, authorId);
        this.fullCv = fullCv;

        construct();
    }

    //main wicket mapping method
    private void construct() {
        
        add(new Label("message", new ResourceModel("authorDetailPage.title")));

        //TODO - can't find image resource
        add(new Image("authorImage", new ResourceReference(AuthorDetailPage.class, "author_avatar.png")));

        add(new Label("authorName", author.getFirstName() + " " + author.getSureName()));

        //birth or death dates could be unknown - aka null
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        if (author.getBirthdate() != null) {
            add(new Label("birthDate", "*" + sdf.format(author.getBirthdate()) + " - "));
        } else {
            //add(new Label("birthDate", "*" + new ResourceModel("authorDetailPage.unknownDate").getObject().toString() + " - "));
            add(new Label("birthDate", "*Neznámé - "));
        }
        if (author.getDeathDate() != null) {
            add(new Label("deathDate", "†" + sdf.format(author.getDeathDate())));
        } else {
            add(new Label("deathDate", ""));
        }

        //if fullCv was set to true, whole CV will be printed out
        if (author.getCv() != null) {
            if (fullCv) {
                add(new Label("authorCv", author.getCv()));
            } else {
                if (author.getCv().length() > 100) {
                    add(new Label("authorCv", author.getCv().substring(0, 100)));
                }
            }
        } else {
        add(new Label("authorCv", ""));
        }
        
        Link cvLink = new Link("fullCv") {
            @Override
            public void onClick() {
                setResponsePage(new AuthorDetailPage(AuthorDetailPage.this.author.getId(), true));
            }
        };
        add(cvLink);

        //cvLink has to be always created, so we hide it when full CV was printed out or does not exist
        if (fullCv || author.getCv() == null) {
            cvLink.setVisible(false);
        }

        //TODO include BookListPanel - author -> this

    }
}
