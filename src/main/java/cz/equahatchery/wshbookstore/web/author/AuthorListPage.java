/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.equahatchery.wshbookstore.web.author;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.repository.AuthorRepository;
import cz.equahatchery.wshbookstore.web.BasePage;
import cz.equahatchery.wshbookstore.web.homepage.HomePage;
import cz.equahatchery.wshbookstore.web.utils.ClickablePropertyColumn;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.PageParameters;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.injection.web.InjectorHolder;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author HCS
 */
public final class AuthorListPage extends BasePage {

    @SpringBean
    private AuthorRepository repository;
    
    public AuthorListPage() {
           super();
        
        add(new Label("message", new ResourceModel("authorListPage.title")));


        
         InjectorHolder.getInjector().inject(this);

        final IColumn[] columns = new IColumn[3];
        columns[0] = new ClickablePropertyColumn<AuthorEntity>(Model.
                of("Id"), "id") {
            @Override
            protected void onClick(IModel<AuthorEntity> clicked) {
		setResponsePage(new AuthorDetailPage(clicked.getObject().getId()));
                //info("You clicked: " + clicked.getObject().getId());
            }
        };
        columns[1] = new PropertyColumn(new Model("firstName"), "firstName", "firstName");
        columns[2] = new PropertyColumn(new Model("sureName"), "sureName", "sureName");
        


        add(new DefaultDataTable<AuthorEntity>("data", columns, new SortableDataProviderImpl(), 25));
    }

    class SortableDataProviderImpl extends SortableDataProvider<AuthorEntity> {

        public SortableDataProviderImpl() {
            setSort(new SortParam("sureName", true));
        }

        public Iterator<? extends AuthorEntity> iterator(int first, int count) {
            final String sortProperty = getSort().getProperty();
            final boolean ascending = getSort().isAscending();
            final List<AuthorEntity> authors = repository.list(first, count, sortProperty, ascending);
            return authors.iterator();
        }

        public int size() {
            return (int) repository.count();
        }

        public IModel<AuthorEntity> model(final AuthorEntity object) {
            return new AbstractReadOnlyModel<AuthorEntity>() {
                @Override
                public AuthorEntity getObject() {
                    return (AuthorEntity) object;
                }
            };
        }
    } 
        
        
    
    
   
}
