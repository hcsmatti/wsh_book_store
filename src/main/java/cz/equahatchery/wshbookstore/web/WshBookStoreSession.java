package cz.equahatchery.wshbookstore.web;

import cz.equahatchery.wshbookstore.model.Basket;
import javax.inject.Inject;
import org.apache.wicket.Request;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebSession;

/**
 * Book Stroe Session Controller with injected Basket Bean
 * @author WSH
 */
public class WshBookStoreSession extends WebSession {

    public static WshBookStoreSession get() {
        return (WshBookStoreSession) Session.get();
    }
    
    @Inject
    private Basket basket;

    /**
     * Construtor
     * @param request Request 
     */
    public WshBookStoreSession(Request request) {
        super(request);

    }

    
    
}
