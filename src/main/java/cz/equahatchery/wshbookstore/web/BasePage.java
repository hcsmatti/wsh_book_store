package cz.equahatchery.wshbookstore.web;

import cz.equahatchery.wshbookstore.web.author.AuthorListPage;
import cz.equahatchery.wshbookstore.web.basketpanel.BasketPanel;
import cz.equahatchery.wshbookstore.web.booklistpanel.BookListPanel;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.CSSPackageResource;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.ResourceModel;

public abstract class BasePage extends WebPage {

    public BasePage() {
        super();
       
        // html head
        add(CSSPackageResource.getHeaderContribution(BasePage.class, "style.css"));

        // header
        ExternalLink headerTitleLink = new ExternalLink("headerTitleLink", new ResourceModel("header.titleLink"));
        headerTitleLink.add(new Label("headerTitleText", new ResourceModel("header.titleText")));
        headerTitleLink.add(new Label("headerTitleDescription", new ResourceModel("header.titleDescription")));
        add(headerTitleLink);
        
        // basketPanel
        add(new BasketPanel("basketPanel")); 
        
        // leftmenu
        
        //add(new LanguagePanel(("localePanel")));
        
       
        // ****  Base menu repeater ****
//        final WebMarkupContainer baseMenuContainer = new WebMarkupContainer("baseMenuContainer");
//        RepeatingView rv = new RepeatingView("leftMenuItemRepeater");
//        for (int i = 1; i <= 8; i++) {
//            rv.add((new ExternalLink(rv.newChildId(), new ResourceModel("leftMenu.item" + i + "Link")))
//                    .add(new Label("leftMenuItemLabel", new ResourceModel("leftMenu.item" + i + "Text"))));
//        }
//
//        baseMenuContainer.setOutputMarkupPlaceholderTag(true);
//        baseMenuContainer.setVisible(true);
//        add(baseMenuContainer);
//        baseMenuContainer.add(rv);

        // author list link
        add(new Link("authorListLink") {
            @Override
            public void onClick() {
                setResponsePage(AuthorListPage.class);
            }
        });
        
        // categories list
        for (int i=1; i<=8; i++) {
    	    final ExternalLink categoryLink = new ExternalLink("categoryLink"+i, new ResourceModel("categories.category"+i+"Link"));
            categoryLink.add(new Label("categoryLinkLabel"+i, new ResourceModel("categories.category"+i+"Text")));
            add(categoryLink);
        }

        add(new BookListPanel("rightPanel"));

        add(new FeedbackPanel("feedback"));



    }
    
    public String togleCaption() {
        final String caption = "";
        
        return caption;
    }

  
}
