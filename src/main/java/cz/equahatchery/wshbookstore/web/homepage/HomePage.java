package cz.equahatchery.wshbookstore.web.homepage;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import cz.equahatchery.wshbookstore.dao.repository.BookRepository;
import cz.equahatchery.wshbookstore.web.BasePage;
import cz.equahatchery.wshbookstore.web.book.BookDetailPage;
import cz.equahatchery.wshbookstore.web.utils.ClickablePropertyColumn;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.injection.web.InjectorHolder;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class HomePage extends BasePage {

    @SpringBean
    private BookRepository repository;
    
   
    public HomePage() {
        super();
        
        
        add(new Label("message", new ResourceModel("homePage.title")));

 
         InjectorHolder.getInjector().inject(this);

        final IColumn[] columns = new IColumn[2];
        columns[0] = new ClickablePropertyColumn<BookEntity>(Model.
                of("Id"), "id") {
            @Override
            protected void onClick(IModel<BookEntity> clicked) {
//                PageParameters pageParameters = new PageParameters();
//		pageParameters.add("id", clicked.getObject().getId().toString());
//		setResponsePage(HomePage.class);
                setResponsePage(new BookDetailPage(clicked.getObject().getId()));
                //info("You clicked: " + clicked.getObject().getId());
            }
        };
        columns[1] = new PropertyColumn(new Model("name"), "name", "name");
        


        add(new DefaultDataTable<BookEntity>("data", columns, new SortableDataProviderImpl(), 25));
    }

    class SortableDataProviderImpl extends SortableDataProvider<BookEntity> {

        public SortableDataProviderImpl() {
            setSort(new SortParam("name", true));
        }

        public Iterator<? extends BookEntity> iterator(int first, int count) {
            final String sortProperty = getSort().getProperty();
            final boolean ascending = getSort().isAscending();
            String cat;
            final List<BookEntity> books;
            if ((cat=getRequest().getParameter("category")) == null) {
                books = repository.list(first, count, sortProperty, ascending);
            } else {
                books = repository.list(first, count, sortProperty, ascending,cat);
            }
            return books.iterator();
        }

        public int size() {
            return (int) repository.count();
        }

        public IModel<BookEntity> model(final BookEntity object) {
            return new AbstractReadOnlyModel<BookEntity>() {
                @Override
                public BookEntity getObject() {
                    return (BookEntity) object;
                }
            };
        }
    }
        
    
    
}