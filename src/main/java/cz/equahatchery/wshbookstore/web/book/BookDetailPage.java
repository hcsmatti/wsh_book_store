package cz.equahatchery.wshbookstore.web.book;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import cz.equahatchery.wshbookstore.dao.repository.BookRepository;
import cz.equahatchery.wshbookstore.web.BasePage;
import cz.equahatchery.wshbookstore.web.buypanel.BuyPanel;
import java.util.Set;
import org.apache.wicket.ResourceReference;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author Jan Jedlicka <jedlija@gmail.com>
 */
public class BookDetailPage extends BasePage {

    @SpringBean
    private BookRepository repository;
    private BookEntity book;
    private AuthorEntity author;

    public BookDetailPage(long bookId) {
        super();

        book = repository.get(BookEntity.class, bookId);

        add(new Label("bookName", book.getName()));

        //TODO - can't find image resource
        add(new Image("bookImage", new ResourceReference(BookDetailPage.class, "book_cover_detail.jpg")));

        Set<AuthorEntity> authors = book.getAuthors();

        //RepeatingView authorView = new RepeatingView("authorNameLink");
        /*
         for (AuthorEntity auth : authors) {

         this.author = auth;

         authorView.add(new Link(authorView.newChildId()) {
         @Override
         public void onClick() {
         setResponsePage(new AuthorDetailPage(BookDetailPage.this.author.getId()));
         }
         }).add(new Label("authorName", author.getFirstName() + " " + author.getSureName()));
         }*/
        //add(authorView);
        if (book.getAnnotation() != null) {
            add(new Label("bookAnnotation", book.getAnnotation()));
        } else {
            add(new Label("bookAnnotation", ""));
        }

        if (book.getDescription() != null) {
            add(new Label("description", book.getDescription()));
        } else {
            add(new Label("description", ""));
        }

        if (book.getSample() != null) {
            add(new ExternalLink("snippetLink", book.getSample()).add(new Label("snippetText", new ResourceModel("bookDetailPage.snippetText"))));
        } else {
            add(new ExternalLink("snippetLink", "").add(new Label("snippetText", new ResourceModel("bookDetailPage.snippetText"))));
        }
        
        add(new BuyPanel("buyPanel", book));
    }
}
