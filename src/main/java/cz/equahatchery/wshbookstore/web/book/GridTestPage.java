package cz.equahatchery.wshbookstore.web.book;
import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import cz.equahatchery.wshbookstore.web.BasePage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.GridView;
import org.apache.wicket.markup.repeater.data.IDataProvider;

/**
 * page for demonstrating the gridview component
 * 
 * @author WSH
 */
public class GridTestPage extends BasePage
{
    /**
     * Constructor
     */
    public GridTestPage()
    {
        IDataProvider<BookEntity> dataProvider = new BookEntityDataProvider();
        GridView<BookEntity> gridView = new GridView<BookEntity>("rows", dataProvider)
        {
            @Override
            protected void populateItem(Item<BookEntity> item)
            {
                final BookEntity bookentity = item.getModelObject();
                item.add(new Label("name", bookentity.getName() + " " +
                    bookentity.getIsbn()));
            }

            @Override
            protected void populateEmptyItem(Item<BookEntity> item)
            {
                item.add(new Label("firstName", "*empty*"));
            }
        };

        gridView.setRows(4);
        gridView.setColumns(3);

        add(gridView);
        add(new PagingNavigator("navigator", gridView));
    }
}