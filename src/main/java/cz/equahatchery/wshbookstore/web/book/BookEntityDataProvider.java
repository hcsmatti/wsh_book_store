/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.equahatchery.wshbookstore.web.book;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import cz.equahatchery.wshbookstore.dao.repository.BookRepository;
import java.util.Iterator;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author WSH
 */
class BookEntityDataProvider implements IDataProvider<BookEntity> {

    @SpringBean
    private BookRepository repository;

    public BookEntityDataProvider() {
    }

    public Iterator<? extends BookEntity> iterator(int i, int i1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int size() {
        return (int) repository.count();
     }

    public IModel<BookEntity> model(BookEntity t) {
        return null;
    }

    public void detach() {
        }
}
