package cz.equahatchery.wshbookstore.web.booklistpanel;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.util.Arrays;
import java.util.List;
import javax.swing.text.html.ListView;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;

/**
 *
 * @author Tomáš Kaniok
 * 
 * Show last five books and top five books
 * 
 */
public final class BookListPanel extends Panel {

    public BookListPanel(String id) {
        super(id);

        
        List<String> listTopValues = Arrays.asList(new String[] { "a", "b", "c", "d", "e" });;
        IDataProvider<String> idataProviderTop = new ListDataProvider(listTopValues);
        
        DataView<String> topView = new DataView<String>("listTop", idataProviderTop) {

            @Override
            protected void populateItem(Item<String> item) {
                item.add(new Label("top", item.getModelObject()));
            }
            
        };
        add(topView);
        
        List<String> listLastValues = Arrays.asList(new String[] { "a", "b", "c", "d", "e" });;
        IDataProvider<String> idataProviderLast = new ListDataProvider(listLastValues);
        
        DataView<String> lastView = new DataView<String>("listLast", idataProviderLast) {

            @Override
            protected void populateItem(Item<String> item) {
                item.add(new Label("last", item.getModelObject()));
            }
            
        };
        add(lastView);
    }
}
