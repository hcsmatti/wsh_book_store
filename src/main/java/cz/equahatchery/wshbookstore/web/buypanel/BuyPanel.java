package cz.equahatchery.wshbookstore.web.buypanel;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import cz.equahatchery.wshbookstore.model.Basket;
import org.apache.wicket.Page;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author Tomáš Kaniok
 * 
 * Panel to buy a book or change number of books
 * 
 */
public final class BuyPanel extends Panel {

    @SpringBean
    private Basket basket;
    
    // Textfield with number of books
    //final TextField<Integer> count = new TextField("count");
    
    public BuyPanel(String id, final BookEntity book) {
        super(id);
        
        //Integer itemQuantity = basket.getItemQuantity(book.getId());
        //count.add(new SimpleAttributeModifier("value", itemQuantity.toString()));
        //count.add(new CountValidator());
        
        /*
        // Button to add one book
        Button addOne = new Button("addOne") {
            @Override
            public void onSubmit() {
                // get value from textfield
                Integer countVal = Integer.parseInt(count.getValue());
                countVal++;
                // and get value back
                count.add(new SimpleAttributeModifier("value", countVal.toString()));
            }
        };
        
        // Button to remove one book
        Button remOne = new Button("remOne") {
            @Override
            public void onSubmit() {
                // get value from textfield
                Integer countVal = Integer.parseInt(count.getValue());
                countVal--;
                // and get value back
                count.add(new SimpleAttributeModifier("value", countVal.toString()));
            }
        };
        */
        
        
        // form
        Form form = new Form("changeCountForm") {
            @Override
            protected void onSubmit () {
                // get value from textfield
                //Integer countVal = Integer.parseInt(count.getValue());
                Integer countVal = basket.getItemQuantity(book.getId());
                countVal++;
                basket.removeItem(book.getId());
                basket.putItem(book, countVal);
//                Class<? extends Page> pageClass = getPage().getClass();
//                setResponsePage(pageClass);
            }
        };
        
        // add buttons and textfield to form
        form.add(new Button("orderButton"));
        //form.add(count);
        //form.add(addOne);
        //form.add(remOne);
        
        // add form to page
        add(form);        
    }
}
