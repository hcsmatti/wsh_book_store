package cz.equahatchery.wshbookstore.web.buypanel;

import org.apache.wicket.validation.CompoundValidator;

/**
 *
 * @author Tomáš Kaniok
 */
public class CountValidator extends CompoundValidator<Integer> {
    
    private static final long serialVersionUID = 1L;
    
    public CountValidator() {
        // Max count of book is 99 and count is a number
        //add(new PatternValidator("0-9{2}"));
    }
}
