package cz.equahatchery.wshbookstore.web.basketpanel;

import cz.equahatchery.wshbookstore.model.Basket;
import org.apache.wicket.ResourceReference;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author Tomáš Kaniok
 * 
 * Basket panel to show information about basket on every page.
 * 
 */
public final class BasketPanel extends Panel {
    
    @SpringBean
    private Basket basket;
    
    public BasketPanel(String id) {
        super(id);
        
        // get total price of basket
        Integer price;
        price = basket.getTotal();
        
        //add(new Label("shoppingCartTitle", new ResourceModel("basketPanel.shoppingCartTitle")));
        add(new Label("sum", new ResourceModel("basketPanel.sum")));
        // add new label with price from basket
        add(new Label("sumPriceValue", price.toString()));
        
        // external link to shopping cart
        ExternalLink shoppingCart = new ExternalLink("shoppingCartLink", new ResourceModel("basketPanel.shoppingCartLink"));
        Label shoppingCartLabel = new Label("shoppingCartLabel");
        shoppingCart.add(shoppingCartLabel);
        add(shoppingCart);
    }
}
