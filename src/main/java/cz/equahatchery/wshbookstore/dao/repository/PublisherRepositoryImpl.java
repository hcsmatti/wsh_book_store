package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.PublisherEntity;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kaniok Tomáš
 */
@Repository
public class PublisherRepositoryImpl extends RepositoryBase<PublisherEntity> implements PublisherRepository {
    
}
