package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.springframework.stereotype.Repository;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for BookEntity
 * @author WSH
 * @author Petr Fuka
 */
@Repository
public class BookRepositoryImpl extends RepositoryBase<BookEntity> implements BookRepository {
    
    /**
     * Queries books from the database, for paging and sorting
     * @param first index of first queried entity returned
     * @param count number of returned books
     * @param sortProperty column to be sorted by
     * @param ascending if the sorting should be ascending (or descending)
     * @return list of sorted books.
     */
    @Transactional
    public List<BookEntity> list(int first, int count, String sortProperty, boolean ascending) {
        final Criteria criteria = getSession().createCriteria(BookEntity.class);
        
        criteria.addOrder(ascending ? Order.asc(sortProperty) : Order.desc(sortProperty));
        criteria.setFirstResult(first);
        criteria.setMaxResults(count);
        
        final List<BookEntity> books = criteria.list();
                
        return books;
    }
    
    /**
     * Number of books in the database
     * @return number of books in the database
     */
    @Transactional
    public long count() {
        final Query query = getSession().createQuery("select count(*) from BookEntity");
        final Long count = (Long) query.uniqueResult();
        return count;
    }

    /**
     * Queries books from the database filtered by category, list
     * sorted and paged.
     * @param first index of first queried entity returned
     * @param count number of returned books
     * @param sortProperty column to be sorted by
     * @param ascending if the sorting should be ascending (or descending)
     * @param filter category to be filtered by.
     * @return list of sorted books.
     */
    @Transactional
    public List<BookEntity> list(int first, int count, String sortProperty, boolean ascending, String filter) {

        final Criteria criteria = getSession().createCriteria(BookEntity.class)
                .createCriteria("categories","cat",JoinType.INNER_JOIN).add(Restrictions.eq("cat.name", filter));
        
        criteria.addOrder(ascending ? Order.asc(sortProperty) : Order.desc(sortProperty));
        criteria.setFirstResult(first);
        criteria.setMaxResults(count);
        
        final List<BookEntity> books = criteria.list();
       
        
        return books;
    }
    
    
}
