package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.TagEntity;

/**
 *
 * @author WSH
 */
public interface TagRepository extends Repository<TagEntity>  {
    
}
