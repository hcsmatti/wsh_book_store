package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.CategoryEntity;
import org.springframework.stereotype.Repository;

/**
 * Repository to handle Categories
 * @author Tomáš Kaniok
 */
@Repository
public class CategoryRepositoryImpl extends RepositoryBase<CategoryEntity> implements CategoryRepository {
    
}
