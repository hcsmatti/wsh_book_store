package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tomas
 */
@Repository
public class AuthorRepositoryImpl extends RepositoryBase<AuthorEntity> implements AuthorRepository {
    
    /**
     * Used in listable Datatable
     * @param first
     * @param count
     * @param sortProperty
     * @param ascending
     * @return 
     */
    @Transactional
    public List<AuthorEntity> list(int first, int count, String sortProperty, boolean ascending) {
        final Criteria criteria = getSession().createCriteria(AuthorEntity.class);
        criteria.addOrder(ascending ? Order.asc(sortProperty) : Order.desc(sortProperty));
        criteria.setFirstResult(first);
        criteria.setMaxResults(count);
        final List<AuthorEntity> users = criteria.list();
        return users;
    }
    
    /**
     * Count all books
     * @return 
     */
    @Transactional
    public long count() {
        final Query query = getSession().createQuery("select count(*) from AuthorEntity");
        final Long count = (Long) query.uniqueResult();
        return count;
    }

     @Transactional
    public AuthorEntity findAuthor(String keyWord) {
        final Query query = getSession().createQuery("from AuthorEntity where firstName like %:key1% or sureName like %:key1% ");
        query.setParameter("key1", keyWord);
        final AuthorEntity user = (AuthorEntity) query.uniqueResult();
        return user;
    }

}
