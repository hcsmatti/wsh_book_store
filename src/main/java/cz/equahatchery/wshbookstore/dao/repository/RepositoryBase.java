package cz.equahatchery.wshbookstore.dao.repository;

import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Abstract class Repository Implementation
 * @author HCS
 * @param <T> 
 */
public abstract class RepositoryBase<T extends Serializable> implements Repository<T> {

    @Inject
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Not needed to current funcionality
     * @param entity 
     */
    @Transactional
    @Override
    public final void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public final T get(Class<T> clazz, long id) {
        return (T) getSession().get(clazz, id);
    }

    /**
     * Not needed to current funcionality
     * @param entity 
     */
    @Transactional
    @Override
    public final void delete(T entity) {
        getSession().delete(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public final List<T> list(Class<T> clazz) {
        final Criteria criteria = getSession().createCriteria(clazz);
        return criteria.list();
    }
}