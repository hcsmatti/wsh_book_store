package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.CategoryEntity;

/**
 *
 * @author Tomáš Kaniok
 */
public interface CategoryRepository extends Repository<CategoryEntity> {
    
}
