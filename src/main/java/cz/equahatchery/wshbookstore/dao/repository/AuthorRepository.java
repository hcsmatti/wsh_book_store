package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.AuthorEntity;
import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.util.List;

/**
 *
 * @author Tomáš Kaniok
 */
public interface AuthorRepository extends Repository<AuthorEntity> {

    long count();
    
    public AuthorEntity findAuthor(String authorSureName);

    List<AuthorEntity> list(int first, int count, String sortProperty, boolean ascending);
}
