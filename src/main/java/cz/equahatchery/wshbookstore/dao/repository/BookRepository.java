package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.BookEntity;
import java.util.List;

/**
 *
 * @author Tomáš Kaniok
 * @author Petr Fuka
 */
public interface BookRepository extends Repository<BookEntity> {
    
    /**
     * Number of books in the database
     * @return number of books in the database
     */
    long count();
     //public BookEntity findBook(String book);

    /**
     * Queries books from the database, for paging and sorting
     * @param first index of first queried entity returned
     * @param count number of returned books
     * @param sortProperty column to be sorted by
     * @param ascending if the sorting should be ascending (or descending)
     * @return list of sorted books.
     */
    List<BookEntity> list(int first, int count, String sortProperty, boolean ascending);
    
    /**
     * Queries books from the database filtered by one parameter, list
     * sorted and paged.
     * @param first index of first queried entity returned
     * @param count number of returned books
     * @param sortProperty column to be sorted by
     * @param ascending if the sorting should be ascending (or descending)
     * @param filter filter for the query, applied to several columns
     * @return list of sorted and filtered books.
     */
    List<BookEntity> list(int first, int count, String sortProperty, boolean ascending,String filter);
    
}
