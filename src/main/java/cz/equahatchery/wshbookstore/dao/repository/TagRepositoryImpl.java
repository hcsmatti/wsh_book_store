package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.TagEntity;
import org.springframework.stereotype.Repository;
/**
 *
 * @author WSH
 */
@Repository
public class TagRepositoryImpl extends RepositoryBase<TagEntity> implements TagRepository  {
    
}
