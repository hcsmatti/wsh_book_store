package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.CountryEntity;

/**
 *
 * @author WSH
 */
public interface CountryRepository extends Repository<CountryEntity>{
    
}
