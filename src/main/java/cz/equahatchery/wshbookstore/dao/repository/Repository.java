package cz.equahatchery.wshbookstore.dao.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Base interface to work with entities
 * @author WSH
 * @param <T> Given Entity trough specific repository
 */
public interface Repository<T extends Serializable> {

    /**
     * Not needed to current funcionality
     * @param entity 
     */
    void delete(T entity);

    T get(Class<T> clazz, long id);

    List<T> list(Class<T> clazz);

    /**
     * Not needed to current funcionality
     * @param entity 
     */
    void saveOrUpdate(T entity);
}
