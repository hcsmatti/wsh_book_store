package cz.equahatchery.wshbookstore.dao.repository;

import cz.equahatchery.wshbookstore.dao.entity.PublisherEntity;

/**
 *
 * @author Tomáš Kaniok
 */
public interface PublisherRepository extends Repository<PublisherEntity> {
    
}
