package cz.equahatchery.wshbookstore.dao.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * country entity
 * @author User
 */
@Entity
@Table(name="Country")
public class CountryEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String shortNameCze;

    private String shortNameEng;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<AddressEntity> address;

    public Set<AddressEntity> getAddress() {
        return address;
    }

    public void setAddress(Set<AddressEntity> address) {
        this.address = address;
    }
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortNameCze() {
        return shortNameCze;
    }

    public void setShortNameCze(String shortNameCze) {
        this.shortNameCze = shortNameCze;
    }

    public String getShortNameEng() {
        return shortNameEng;
    }

    public void setShortNameEng(String shortNameEng) {
        this.shortNameEng = shortNameEng;
    }

    
    
}
